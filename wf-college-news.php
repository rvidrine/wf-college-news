<?php
    /* create a custom post type of news, attached to the init hook.*/
  if (!function_exists('wfco_create_news_post_type')) {

    add_action( 'init', 'wfco_create_news_post_type' );
    function wfco_create_news_post_type() {
    
        $args = array(
          'labels' => array(
          'name' => __( 'News Items' ),
          'singular_name' => __( 'News Item' ),
          'add_new_item' => __( 'Add News Item' ),
          'edit_item' => __( 'Edit News Item' ),
          'new_item' => __( 'New News Item' ),
          'search_items' => __( 'Search News' )
          ),
          'public' => true,
          'has_archive' => 'news',
          'rewrite' => array('slug' => 'news'),
          'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions', 'comments', 'trackbacks'),
          'taxonomies' => array('news_type', 'post_tag'),
          'menu_position' => 20
      );
          register_post_type( 'wfco_dept_news', $args );
    }
    /* Create a custom taxonomy of news type and add it to the news post type*/
    $args = array(
      'label' => 'News Type',
      'hierarchical' => true
      );
      register_taxonomy('news_type', 'News Items', $args);
  }

