<?php
/***
* After placing the wf-college-news.php file in your theme's inc folder,
* add the contents of this file to your functions.php.
* NOTE: If you are not writing your own parent theme, and just want to 
* add this to your child theme, change the require line to
* require get_stylesheet_directory() . '/inc/wf-college-news.php';
* and put wf-college-news.php in a folder called inc in your child theme
* folder (or change the path in the require statement above).
*
Add the Department News Post Type
slug: wfco_dept_news
***/
require get_template_directory() . '/inc/wf-college-news.php';
